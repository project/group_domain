<?php

declare(strict_types=1);

namespace Drupal\group_domain_test\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Stop the request before the controller is fired.
 */
final class GroupDomainTestSubscriber implements EventSubscriberInterface {

  /**
   * Stop processing.
   */
  public function stopProcessing(RequestEvent $event): void {
    $event->setResponse(new Response());
  }

  /**
   * {@inheritdoc}
   *
   * @return mixed[]
   *   The events.
   */
  public static function getSubscribedEvents(): array {
    return [
      KernelEvents::REQUEST => [
        ['stopProcessing', -10000],
      ],
    ];
  }

}
