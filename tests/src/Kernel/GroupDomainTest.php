<?php

declare(strict_types=1);

namespace Drupal\Tests\group_domain\Kernel;

use Drupal\field\Entity\FieldConfig;
use Drupal\group\Entity\GroupInterface;
use Drupal\group\PermissionScopeInterface;
use Drupal\group_domain\GroupDomainInfo;
use Drupal\user\RoleInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Tests\group\Kernel\GroupKernelTestBase;

/**
 * Test description.
 *
 * @group group_domain
 */
final class GroupDomainTest extends GroupKernelTestBase {

  /**
   * The domain being tested.
   */
  private const TEST_DOMAIN = 'groups1.example.com';

  /**
   * {@inheritdoc}
   *
   * @todo Change this to protected when the parent class does.
   */
  protected static $modules = [
    'group_domain',
    'group_domain_test',
  ];

  /**
   * The group being tested.
   */
  protected ?GroupInterface $group;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->container->get('router.builder')->rebuild();
    $this->installConfig(['group_domain']);

    $group_type = $this->createGroupType([
      'creator_membership' => TRUE,
    ]);

    FieldConfig::create([
      'field_name' => GroupDomainInfo::DOMAIN_FIELD_NAME,
      'entity_type' => 'group',
      'bundle' => $group_type->id(),
    ])->save();

    $group_role = $this->createGroupRole([
      'group_type' => $group_type->id(),
      'admin' => TRUE,
      'scope' => PermissionScopeInterface::INDIVIDUAL_ID,
      'global_role' => RoleInterface::AUTHENTICATED_ID,
    ]);
    $group_type->set('creator_roles', [$group_role->id()]);
    $group_type->save();

    $this->group = $this->createGroup([
      'type' => $group_type->id(),
      GroupDomainInfo::DOMAIN_FIELD_NAME => self::TEST_DOMAIN,
    ]);
  }

  /**
   * Test callback.
   *
   * @covers PathProcessor::processOutbound()
   * @covers PathRequestSubscriber::onResponse()
   *
   * @dataProvider incomingProvider
   */
  public function testDomainIncoming(string $template, int $expectedStatusCode): void {
    // This tests PathProcessor:
    $groupContents = $this->group->getRelationships('group_membership');
    $url = \strtr($template, [
      ':domain' => self::TEST_DOMAIN,
      ':group_id' => $this->group->id(),
      ':content_id' => \reset($groupContents)->id(),
    ]);
    $request = Request::create($url);
    $response = $this->container->get('http_kernel')->handle($request);
    self::assertSame($expectedStatusCode, $response->getStatusCode());
  }

  /**
   * Test callback.
   *
   * @covers PathProcessor::processInbound()
   * @covers PathRequestSubscriber::onResponse()
   */
  public function testDomainOutcoming(): void {
    $scheme = $this->container->get('request_stack')->getCurrentRequest()->getScheme();
    $url = $this->group->toUrl();
    $url->setOption('path_processing', FALSE);
    self::assertSame(\sprintf('/group/%d', $this->group->id()), $url->toString());

    $url->setOption('path_processing', TRUE);
    self::assertSame(\sprintf('%s://%s/', $scheme, self::TEST_DOMAIN), $url->toString());

    $groupContents = $this->group->getRelationships('group_membership');
    $groupContent = \reset($groupContents);
    $url = $groupContent->toUrl();
    $url->setOption('path_processing', FALSE);
    self::assertSame(\sprintf('/group/%d/content/%d', $this->group->id(), $groupContent->id()), $url->toString());

    $url->setOption('path_processing', TRUE);
    self::assertSame(\sprintf('%s://%s/content/%d', $scheme, self::TEST_DOMAIN, $groupContent->id()), $url->toString());
  }

  /**
   * Data provider.
   *
   * @return mixed[]
   *   Test data.
   */
  public function incomingProvider(): array {
    return [
      // This uses the domain set up, positive result is expected.
      ["https://:domain/content/:content_id", 200],
      // This adds a beginning x to the domain set up, not found is expected.
      ["https://x:domain/group/:group_id", 404],
      // This adds a beginning x to the domain set up, not found is expected.
      ["https://x:domain/group/:group_id/content/:content_id", 404],
      // This adds a beginning x to the domain set up, not found is expected.
      ["https://x:domain/content/:content_id", 404],
    ];
  }

}
