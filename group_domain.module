<?php

/**
 * @file
 * Allows to display groups and their content on custom domains.
 */

declare(strict_types=1);

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\group\Entity\GroupInterface;
use Drupal\group_domain\GroupDomainInfo;
use Drupal\user\UserInterface;

/**
 * Implements hook_ENTITY_TYPE_presave().
 */
function group_domain_group_presave(GroupInterface $group): void {
  if (!$group->hasField(GroupDomainInfo::DOMAIN_FIELD_NAME)) {
    return;
  }
  if ($group->isNew()) {
    if ($group->get(GroupDomainInfo::DOMAIN_FIELD_NAME)->isEmpty()) {
      return;
    }
  }
  else {
    if ($group->get(GroupDomainInfo::DOMAIN_FIELD_NAME)->value === $group->original->get(GroupDomainInfo::DOMAIN_FIELD_NAME)->value) {
      return;
    }
  }

  // If still here, we need to clear the domain info cache
  // and invalidate the render cache.
  \Drupal::cache('data')->delete(GroupDomainInfo::DOMAIN_FIELD_NAME);
  \Drupal::cache('render')->invalidateAll();
}

/**
 * Implements hook_ENTITY_TYPE_insert().
 */
function group_domain_user_insert(UserInterface $user): void {
  // Add newly registered users to domain group if applicable.
  $group_domain_info = \Drupal::service('group_domain.info');

  $domain_group = $group_domain_info->getCurrentDomainGroup();
  if ($domain_group === NULL) {
    return;
  }
  $config_entity = $group_domain_info->getConfigEntityForCurrentDomain($domain_group->bundle());
  if (
    $config_entity === NULL ||
    $config_entity->get('status') === FALSE ||
    $config_entity->get('add_membership_on_registration') !== TRUE
  ) {
    return;
  }

  $domain_group->addMember($user, [
    'group_roles' => $config_entity->get('registration_membership_roles'),
  ]);
}

/**
 * Implements hook_entity_bundle_field_info_alter().
 */
function group_domain_entity_bundle_field_info_alter(&$fields, EntityTypeInterface $entity_type, $bundle): void {
  if (\array_key_exists(GroupDomainInfo::DOMAIN_FIELD_NAME, $fields)) {
    // Use the ID as defined in the annotation of the constraint definition.
    $fields[GroupDomainInfo::DOMAIN_FIELD_NAME]->addConstraint('GroupDomain');
  }
}
