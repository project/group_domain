<?php

declare(strict_types=1);

namespace Drupal\group_domain;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\Core\Update\UpdateKernel;

/**
 * Make sure updb can run.
 */
final class GroupDomainServiceProvider extends ServiceProviderBase {

  private const SERVICE_ID = 'group_domain.path_processor';

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container): void {

    // We never know which theme can be customized per domain, also
    // global renderables can have domain-specific children that may
    // just dissapear otherwise if cached without this context.
    $renderer_config = $container->getParameter('renderer.config');
    $renderer_config['required_cache_contexts'][] = 'group_domain';
    $container->setParameter('renderer.config', $renderer_config);

    // The alias-based processor requires the path_alias entity schema to be
    // installed, so we prevent it from being registered to the path processor
    // manager. We do this by removing the tags that the compiler pass looks
    // for. This means that the URL generator can safely be used during the
    // database update process.
    if ($container->get('kernel') instanceof UpdateKernel && $container->hasDefinition(self::SERVICE_ID)) {
      $container->getDefinition(self::SERVICE_ID)
        ->clearTag('path_processor_inbound')
        ->clearTag('path_processor_outbound');
    }
  }

}
