<?php

namespace Drupal\group_domain\Breadcrumb;

use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\group_domain\GroupDomainInfo;

/**
 * Breadcrumb builder class.
 */
class GroupBreadcrumbBuilder implements BreadcrumbBuilderInterface {

  /**
   * The obvious.
   */
  public function __construct(GroupDomainInfo $group_domain_info) {
    $this->groupDomainInfo = $group_domain_info;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    if ($route_match->getRouteName() === 'entity.group.canonical' && ($group = $route_match->getParameter('group'))) {
      $domain_group = $this->groupDomainInfo->getCurrentDomainGroup();
      if ($domain_group !== NULL && $domain_group->id() === $group->id()) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function build(RouteMatchInterface $route_match) {
    // No breadcrumb.
    $breadcrumb = new Breadcrumb();
    $breadcrumb->addCacheContexts(['route']);
    return $breadcrumb;
  }

}
