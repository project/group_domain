<?php

declare(strict_types=1);

namespace Drupal\group_domain;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Path\PathValidatorInterface;
use Drupal\Core\PathProcessor\InboundPathProcessorInterface;
use Drupal\Core\PathProcessor\OutboundPathProcessorInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Url;
use Drupal\group_domain\Entity\GroupDomainSettings;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Route;

/**
 * Class for identifying group from domain name.
 */
final class PathProcessor implements OutboundPathProcessorInterface, InboundPathProcessorInterface {

  /**
   * The Group Domain Info service.
   */
  private GroupDomainInfo $groupDomainInfo;

  /**
   * The main domain.
   */
  private ?string $mainDomain;

  /**
   * Outbound links ehavior for different group types.
   *
   * @var string[]
   */
  private array $outboundBehavior = [];

  /**
   * Recursive call flag.
   */
  private bool $recursiveCall = FALSE;

  /**
   * The path validator service.
   */
  private ?PathValidatorInterface $pathValidator = NULL;

  /**
   * Constructs a new PathProcessor object.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    GroupDomainInfo $group_domain_info
  ) {
    $this->groupDomainInfo = $group_domain_info;

    $config = $config_factory->get(GroupDomainInfo::CONFIG_NAME);
    $this->mainDomain = $config->get('main_domain');
  }

  /**
   * Inbound links process method.
   */
  public function processInbound($path, Request $request): string {
    if ($request === NULL || $this->recursiveCall) {
      return $path;
    }

    $base_group_path = $this->groupDomainInfo->getBasePathByDomain($request->getHttpHost());
    if ($base_group_path === NULL) {
      return $path;
    }

    // Process front page.
    if ($path === '/') {
      return $base_group_path;
    }

    // Replace unprefixed inbound paths with prefixed ones if valid.
    $group_prefixed_path = $base_group_path . $path;
    if ($this->isValidPath($group_prefixed_path)) {
      $path = $group_prefixed_path;
    }

    return $path;
  }

  /**
   * Outbound links process method.
   */
  public function processOutbound($path, &$options = [], ?Request $request = NULL, ?BubbleableMetadata $bubbleable_metadata = NULL): string {
    if ($request === NULL) {
      return $path;
    }

    $domain = $request->getHttpHost();

    // Links form other domains to the group.
    $route = $options['route'] ?? NULL;
    if ($route instanceof Route && \str_starts_with($route->getPath(), '/group/')) {
      $domain_info = $this->groupDomainInfo->getDomainInfoByPath($path);
      if (\is_array($domain_info) && $domain_info['domain'] !== $domain) {
        // Don't process when not on the main domain and setting is set.
        if (
          $this->mainDomain !== NULL &&
          $domain === $this->mainDomain &&
          $this->ignoreMainDomainOutbound($domain_info['group_type'])
        ) {
          return $path;
        }

        $domain = $domain_info['domain'];
        $options['base_url'] = $request->getScheme() . '://' . $domain;
        $options['absolute'] = TRUE;
      }
    }

    $base_group_path = $this->groupDomainInfo->getBasePathByDomain($domain);

    // Don't process for home links.
    if ($base_group_path === NULL || $path === '/') {
      return $path;
    }

    // Process home page.
    if ($path === $base_group_path) {
      return '/';
    }

    // Process other paths - remove prefix.
    if (\str_starts_with($path, $base_group_path . '/')) {
      $path = \substr($path, \strlen($base_group_path));
    }

    return $path;
  }

  /**
   * Should we ignore main domain outbouund links for the given group type?
   */
  private function ignoreMainDomainOutbound(string $group_type_id): bool {
    if (!\array_key_exists($group_type_id, $this->outboundBehavior)) {
      $config_entity = $this->groupDomainInfo->getConfigEntity($group_type_id);
      if ($config_entity instanceof GroupDomainSettings) {
        $this->outboundBehavior[$group_type_id] = $config_entity->get('outbound_behavior');
      }
      else {
        $this->outboundBehavior[$group_type_id] = GroupDomainInfo::IGNORE_MAIN_DOMAIN_OUTBOUND;
      }
    }

    return $this->outboundBehavior[$group_type_id] === GroupDomainInfo::IGNORE_MAIN_DOMAIN_OUTBOUND;
  }

  /**
   * Check if path is valid.
   *
   * This method will call all the path processors (including this one).
   * Sufficient protection against recursive calls is needed.
   */
  private function isValidPath(string $path): bool {
    if ($this->recursiveCall) {
      return FALSE;
    }
    $this->recursiveCall = TRUE;
    $path = $this->getPathValidator()->getUrlIfValidWithoutAccessCheck($path);
    $this->recursiveCall = FALSE;

    return $path instanceof Url;
  }

  /**
   * Path validator setter.
   */
  public function setPathValidator(PathValidatorInterface $path_validator): self {
    $this->pathValidator = $path_validator;

    return $this;
  }

  /**
   * Path validator getter.
   */
  private function getPathValidator(): PathValidatorInterface {
    if ($this->pathValidator === NULL) {
      // Normal dependency injection won't work for path validator as it causes
      // a circular dependency issue.
      // phpcs:disable
      // @phpstan-ignore-next-line
      $this->setPathValidator(\Drupal::service('path.validator'));
      // phpcs:enable
    }

    return $this->pathValidator;
  }

}
