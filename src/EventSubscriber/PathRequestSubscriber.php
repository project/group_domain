<?php

declare(strict_types=1);

namespace Drupal\group_domain\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Site\Settings;
use Drupal\group\Entity\GroupInterface;
use Drupal\group\Entity\GroupRelationshipInterface;
use Drupal\group\Entity\Storage\GroupRelationshipStorageInterface;
use Drupal\group_domain\GroupDomainInfo;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Path subscriber for controller requests.
 */
final class PathRequestSubscriber implements EventSubscriberInterface {

  /**
   * The ETM.
   */
  private GroupRelationshipStorageInterface $groupContentStorage;

  /**
   * The RM.
   */
  private RouteMatchInterface $routeMatch;

  /**
   * The domain info service.
   */
  private GroupDomainInfo $domainInfo;

  /**
   * The main domain.
   */
  private ?string $mainDomain;

  /**
   * Constructor.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    RouteMatchInterface $route_match,
    ConfigFactoryInterface $config_factory,
    GroupDomainInfo $domain_info
  ) {
    $this->groupContentStorage = $entity_type_manager->getStorage('group_content');
    $this->routeMatch = $route_match;
    $this->domainInfo = $domain_info;

    $config = $config_factory->get(GroupDomainInfo::CONFIG_NAME);
    $this->mainDomain = $config->get('main_domain');
  }

  /**
   * Special handling for redirect module.
   *
   * @todo Conditionally register this or factor it out to somewhere else.
   */
  public function onRequest(RequestEvent $event): void {
    if (Settings::get('group_domain_mapping') !== NULL) {
      $request = $event->getRequest();
      $request->attributes->set('_disable_route_normalizer', TRUE);
    }
  }

  /**
   * Handle response.
   *
   * Throw HTTP not found if request is for content which is not nested under
   * the right custom domain.
   */
  public function onResponse(ResponseEvent $event): void {
    if (!$event->isMainRequest()) {
      return;
    }

    // @todo Remove that killswitch if it doesn't serve any purpose.
    if (Settings::get('group_domain.custom_domain_kill', FALSE) !== FALSE) {
      @\trigger_error("Using killswitch is deprecated in 1.0.x and shouldn't be relied on unless a proper use case is found.", \E_USER_DEPRECATED);
      return;
    }

    $request = $event->getRequest();

    // Don't act on the main domain as well, all should be accessible there.
    if ($this->mainDomain !== NULL && $request->getHttpHost() === $this->mainDomain) {
      return;
    }

    $domain_group = $this->domainInfo->getRequestDomainGroup($request);
    $domain_group_id = $domain_group === NULL ? '0' : $domain_group->id();

    $parameters = $this->routeMatch->getParameters()->all();
    if (\count($parameters) === 0) {
      return;
    }

    $throw = FALSE;
    foreach ($parameters as $parameter) {
      if ($parameter instanceof GroupInterface) {
        if ($parameter->id() === $domain_group_id) {
          return;
        }
        // Group as group content case.
        $group_contents = $this->groupContentStorage->loadByEntity($parameter);
        foreach ($group_contents as $group_content) {
          if ($group_content->getGroup()->id() === $domain_group_id) {
            return;
          }
        }
      }
      elseif ($parameter instanceof GroupRelationshipInterface) {
        if ($parameter->getGroup()->id() === $domain_group_id) {
          return;
        }
      }

      // Don't throw if we don't have group or group content in parameters.
      else {
        continue;
      }

      $throw = TRUE;
    }

    if ($throw) {
      throw new NotFoundHttpException();
    }
  }

  /**
   * {@inheritdoc}
   *
   * @return array<string, mixed[]>
   *   The events.
   */
  public static function getSubscribedEvents(): array {
    $events = [];
    $events[KernelEvents::RESPONSE][] = ['onResponse'];
    $events[KernelEvents::REQUEST][] = ['onRequest', 40];

    return $events;
  }

}
