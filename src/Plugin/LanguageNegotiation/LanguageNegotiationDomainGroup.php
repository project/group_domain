<?php

declare(strict_types=1);

namespace Drupal\group_domain\Plugin\LanguageNegotiation;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\group_domain\GroupDomainInfo;
use Drupal\language\LanguageNegotiationMethodBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class for identifying language from a selected language.
 *
 * @LanguageNegotiation(
 *   id = Drupal\group_domain\Plugin\LanguageNegotiation\LanguageNegotiationDomainGroup::METHOD_ID,
 *   weight = -3,
 *   name = @Translation("Domain group language"),
 *   description = @Translation("Language based on the current domain group."),
 * )
 */
class LanguageNegotiationDomainGroup extends LanguageNegotiationMethodBase implements ContainerFactoryPluginInterface {

  public const METHOD_ID = 'domain_group_language';

  /**
   * Constructs a new LanguageNegotiationDomainGroup object.
   */
  public function __construct(
    private readonly GroupDomainInfo $groupDomainInfo
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($container->get('group_domain.info'));
  }

  /**
   * {@inheritdoc}
   */
  public function getLangcode(Request $request = NULL): ?string {
    $group = $this->groupDomainInfo->getCurrentDomainGroup();
    if ($group === NULL) {
      return NULL;
    }

    $languages = $group->getTranslationLanguages();
    // If the current domain group has more than 1 language, we cannot rely on
    // that and need to fall back to other methods.
    if (\count($languages) > 1) {
      return NULL;
    }

    $language = \reset($languages);
    return $language->getId();
  }

}
