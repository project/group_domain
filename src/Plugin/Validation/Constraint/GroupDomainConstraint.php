<?php

declare(strict_types=1);

namespace Drupal\group_domain\Plugin\Validation\Constraint;

use Drupal\Core\Validation\Plugin\Validation\Constraint\UniqueFieldConstraint;

/**
 * Checks if an entity field has a unique value.
 *
 * @Constraint(
 *   id = "GroupDomain",
 *   label = @Translation("Group domain field constraint", context = "Validation"),
 * )
 */
final class GroupDomainConstraint extends UniqueFieldConstraint {

  /**
   * Used in the validator for violations instead of the parent message.
   */
  public string $customMessage = 'Group domain field invalid: @details';

  /**
   * {@inheritdoc}
   */
  public function validatedBy(): string {
    return '\Drupal\group_domain\Plugin\Validation\Constraint\GroupDomainValidator';
  }

}
