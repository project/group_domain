<?php

declare(strict_types=1);

namespace Drupal\group_domain\Plugin\Validation\Constraint;

use Drupal\Core\Validation\Plugin\Validation\Constraint\UniqueFieldValueValidator;
use Symfony\Component\Validator\Constraint;

/**
 * Validates Group Domain field values.
 */
final class GroupDomainValidator extends UniqueFieldValueValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($items, Constraint $constraint): void {
    // Field uniqueness validation from parent.
    parent::validate($items, $constraint);

    $item = $items->first();
    if ($item === NULL) {
      return;
    }

    if ($item->value !== \trim($item->value)) {
      $this->context->addViolation($constraint->customMessage, [
        '@details' => 'Please remove spaces.',
      ]);
    }
    if (\str_starts_with($item->value, 'http://') || \str_starts_with($item->value, 'https://')) {
      $this->context->addViolation($constraint->customMessage, [
        '@details' => 'Please remove protocol (no http:// or https://).',
      ]);
    }
    if (\substr($item->value, -1, 1) === '/') {
      $this->context->addViolation($constraint->customMessage, [
        '@details' => 'Please remove the trailing slash.',
      ]);
    }

  }

}
