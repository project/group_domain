<?php

declare(strict_types=1);

namespace Drupal\group_domain\Plugin\views\filter;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\group\Plugin\Group\Relation\GroupRelationTypeManagerInterface;
use Drupal\group_domain\GroupDomainInfo;
use Drupal\views\Plugin\views\filter\FilterPluginBase;
use Drupal\views\Views;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * If active, views will show only entities from the current domain group.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("domain_group_content")
 */
class DomainGroupContent extends FilterPluginBase {

  /**
   * Group Domain Info service.
   */
  private GroupDomainInfo $groupDomainInfo;

  /**
   * Entity Type Manager.
   */
  private EntityTypeManagerInterface $entityTypeManager;

  /**
   * Group Relation Type Manager.
   */
  private GroupRelationTypeManagerInterface $groupRelationTypeManager;

  /**
   * The constructor.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    GroupDomainInfo $group_domain_info,
    EntityTypeManagerInterface $entity_type_manager,
    GroupRelationTypeManagerInterface $group_relation_type_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->groupDomainInfo = $group_domain_info;
    $this->entityTypeManager = $entity_type_manager;
    $this->groupRelationTypeManager = $group_relation_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('group_domain.info'),
      $container->get('entity_type.manager'),
      $container->get('group_relation_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    $contexts = parent::getCacheContexts();
    $contexts[] = 'group_domain';
    return $contexts;
  }

  /**
   * Add this filter to the query.
   */
  public function query(): void {
    $group = $this->groupDomainInfo->getCurrentDomainGroup();

    // Don't filter if not on a group domain.
    if ($group === NULL) {
      return;
    }

    $entity_type_id = $this->getEntityType();
    $table_alias = $this->ensureMyTable();
    if ($entity_type_id === 'group_content') {
      $this->query->addWhere($this->options['group'], $table_alias . '.gid', $group->id(), '=');
      return;
    }

    $group_content_plugin_definitions = array_filter($this->groupRelationTypeManager->getDefinitions(), fn($definition) => $definition->getEntityTypeId() === $entity_type_id);

    $group_content_type_ids = [];
    foreach (\array_keys($group_content_plugin_definitions) as $plugin_id) {
      $group_content_type_ids = \array_unique(\array_merge($group_content_type_ids, $this->groupRelationTypeManager->getRelationshipTypeIds($plugin_id)));
    }
    if (\count($group_content_type_ids) === 0) {
      return;
    }

    $entity_type = $this->entityTypeManager->getDefinition($this->getEntityType());
    $group_content_definition = $this->entityTypeManager->getDefinition('group_content');
    $group_content_data_table = $group_content_definition->getDataTable();

    $configuration = [
      'table' => $group_content_data_table,
      'field' => 'entity_id',
      'left_table' => $table_alias,
      'left_field' => $entity_type->getKey('id'),
      'type' => 'INNER',
      'operator' => '=',
    ];

    $join = Views::pluginManager('join')->createInstance('standard', $configuration);
    $group_content_data_table_alias = $this->query->addRelationship($group_content_data_table, $join, $table_alias);

    $this->query->addWhere($this->options['group'], $group_content_data_table_alias . '.' . $group_content_definition->getKey('bundle'), $group_content_type_ids, 'IN');
    $this->query->addWhere($this->options['group'], $group_content_data_table_alias . '.gid', $group->id(), '=');
  }

}
