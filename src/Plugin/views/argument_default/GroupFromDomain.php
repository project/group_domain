<?php

declare(strict_types=1);

namespace Drupal\group_domain\Plugin\views\argument_default;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\group\Entity\GroupInterface;
use Drupal\group_domain\GroupDomainInfo;
use Drupal\views\Plugin\views\argument_default\ArgumentDefaultPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Default argument plugin to get current domain group ID.
 *
 * @ingroup views_argument_default_plugins
 *
 * @ViewsArgumentDefault(
 *   id = "group_from_domain",
 *   title = @Translation("Group ID from the current domain")
 * )
 */
final class GroupFromDomain extends ArgumentDefaultPluginBase implements CacheableDependencyInterface {

  /**
   * Group Domain Info service.
   */
  private GroupDomainInfo $groupDomainInfo;

  /**
   * Constructs a new GroupFromDomain instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\group_domain\GroupDomainInfo $group_domain_info
   *   Group Domain Info service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    GroupDomainInfo $group_domain_info
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->groupDomainInfo = $group_domain_info;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('group_domain.info')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getArgument(): string {
    $group = $this->groupDomainInfo->getCurrentDomainGroup();
    return $group instanceof GroupInterface ? $group->id() : $this->argument->options['exception']['value'];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return ['group_domain'];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return Cache::PERMANENT;
  }

}
