<?php

declare(strict_types=1);

namespace Drupal\group_domain\Routing;

use Drupal\Core\Routing\RouteProvider;
use Symfony\Component\HttpFoundation\Request;

/**
 * Custom router.route_provider service to make it domain context-sensitive.
 */
final class GroupDomainRouteProvider extends RouteProvider {

  /**
   * Returns the cache ID for the route collection cache.
   *
   * We are overriding the cache id by inserting the host to the cid.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   *
   * @see \Drupal\Core\Routing\RouteProvider::getRouteCollectionCacheId()
   *
   * @return string
   *   The cache ID.
   */
  protected function getRouteCollectionCacheId(Request $request): string {
    $this->addExtraCacheKeyPart('group_domain', $request->getHttpHost());
    return parent::getRouteCollectionCacheId($request);
  }

}
