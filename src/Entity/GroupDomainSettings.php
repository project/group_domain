<?php

declare(strict_types=1);

namespace Drupal\group_domain\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\group\Entity\GroupRoleInterface;
use Drupal\group_domain\GroupDomainInfo;

/**
 * Defines the group_domain_settings config entity type.
 *
 * @ConfigEntityType(
 *   id = "group_domain_settings",
 *   label = @Translation("Group domain settings"),
 *   config_prefix = "group_domain",
 *   admin_permission = "administer group",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *   },
 *   config_export = {
 *     "id",
 *     "status",
 *     "outbound_behavior",
 *     "add_membership_on_registration",
 *     "registration_membership_roles",
 *   },
 * )
 */
final class GroupDomainSettings extends ConfigEntityBase {

  /**
   * The group type ID.
   */
  protected ?string $id;

  /**
   * Behavior of outbound links to other group domains.
   */
  protected string $outbound_behavior = GroupDomainInfo::IGNORE_MAIN_DOMAIN_OUTBOUND;

  /**
   * Should membership be added when user is registered on a group domain?
   */
  protected bool $add_membership_on_registration = FALSE;

  /**
   * Group membership roles to assign upon registration.
   *
   * @var string[]
   */
  protected array $registration_membership_roles = [];

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies(): self {
    parent::calculateDependencies();

    $group_type = $this->entityTypeManager()
      ->getStorage('group_type')
      ->load($this->id);
    $this->addDependency('config', $group_type->getConfigDependencyName());

    foreach ($this->registration_membership_roles as $role_id) {
      $group_role = $this->entityTypeManager()
        ->getStorage('group_role')
        ->load($role_id);
      if ($group_role instanceof GroupRoleInterface) {
        $this->addDependency('config', $group_role->getConfigDependencyName());
      }
    }

    return $this;
  }

  /**
   * Check if settings are empty.
   */
  public function empty(): bool {
    if ($this->status) {
      return FALSE;
    }

    $empty = TRUE;

    if (
      $this->outbound_behavior !== GroupDomainInfo::IGNORE_MAIN_DOMAIN_OUTBOUND ||
      $this->add_membership_on_registration !== FALSE ||
      \count($this->registration_membership_roles) !== 0
    ) {
      $empty = FALSE;
    }

    return $empty;
  }

}
