<?php

declare(strict_types=1);

namespace Drupal\group_domain\Cache\Context;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\Context\CacheContextInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\group_domain\GroupDomainInfo;

/**
 * Defines the GroupDomainCacheContext service, for group domain caching.
 *
 * Cache context ID: 'group_domain'.
 */
final class GroupDomainCacheContext implements CacheContextInterface {

  use StringTranslationTrait;

  /**
   * Group Domain Info service.
   */
  private GroupDomainInfo $groupDomainInfo;

  /**
   * Cache for the context value as it's request-specific only.
   */
  private ?string $contextValue = NULL;

  /**
   * Constructs a new GroupDomainCacheContext object.
   *
   * @param \Drupal\group_domain\GroupDomainInfo $group_domain_info
   *   Group Domain Info service.
   */
  public function __construct(GroupDomainInfo $group_domain_info) {
    $this->groupDomainInfo = $group_domain_info;
  }

  /**
   * {@inheritdoc}
   */
  public static function getLabel() {
    return (string) $this->t('Domain group ID');
  }

  /**
   * {@inheritdoc}
   */
  public function getContext() {
    if ($this->contextValue === NULL) {
      $group = $this->groupDomainInfo->getCurrentDomainGroup();
      $this->contextValue = $group instanceof GroupInterface ? $group->id() : '0';
    }
    return $this->contextValue;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata() {
    return new CacheableMetadata();
  }

}
