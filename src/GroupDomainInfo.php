<?php

declare(strict_types=1);

namespace Drupal\group_domain;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\CacheCollector;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Query\QueryException;
use Drupal\Core\Lock\LockBackendInterface;
use Drupal\Core\Site\Settings;
use Drupal\group\Entity\GroupInterface;
use Drupal\group_domain\Entity\GroupDomainSettings;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class for identifying group from domain name.
 *
 * @see \Drupal\language\Plugin\LanguageNegotiation\LanguageNegotiationUrl
 */
final class GroupDomainInfo extends CacheCollector {

  public const DOMAIN_FIELD_NAME = 'group_domain';
  public const CONFIG_NAME = 'group_domain.settings';
  public const IGNORE_MAIN_DOMAIN_OUTBOUND = 'ignore';
  public const CONVERT_MAIN_DOMAIN_OUTBOUND = 'convert';

  protected EntityTypeManagerInterface $entityTypeManager;
  protected RequestStack $requestStack;

  /**
   * Constructs a new PathProcessor object.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    RequestStack $request_stack,
    CacheBackendInterface $cache,
    LockBackendInterface $lock
  ) {
    parent::__construct(self::DOMAIN_FIELD_NAME, $cache, $lock);

    $this->entityTypeManager = $entity_type_manager;
    $this->requestStack = $request_stack;
  }

  /**
   * Get group type config.
   */
  public function getConfigEntity(string $group_type_id): GroupDomainSettings {
    $group_domain_settings_storage = $this->entityTypeManager->getStorage('group_domain_settings');
    $config_entity = $group_domain_settings_storage->load($group_type_id);
    if (!$config_entity instanceof GroupDomainSettings) {
      $config_entity = $group_domain_settings_storage->create([
        'id' => $group_type_id,
        'status' => FALSE,
      ]);
    }
    return $config_entity;
  }

  /**
   * Get current domain group config entity.
   */
  public function getConfigEntityForCurrentDomain(): ?GroupDomainSettings {
    $group = $this->getCurrentDomainGroup();
    if ($group === NULL) {
      return NULL;
    }

    return $this->getConfigEntity($group->bundle());
  }

  /**
   * Public base path by domain getter.
   */
  public function getBasePathByDomain(string $domain): ?string {
    $value = $this->get($domain);
    return $value === NULL ? NULL : $value[0];
  }

  /**
   * Public domain info by path getter.
   *
   * @return array<string, string>|null
   *   Domain info array.
   */
  public function getDomainInfoByPath(string $path): ?array {
    return $this->get($path);
  }

  /**
   * The information to be set for a domain.
   *
   * @return array<string, string>
   *   Domain and group type array.
   */
  private function domainGroupInfo(string $domain, GroupInterface $group): array {
    return [
      'domain' => $domain,
      'group_type' => $group->bundle(),
    ];
  }

  /**
   * Resolve cache miss.
   *
   * @return string[]|null
   *   Generated value.
   */
  protected function resolveCacheMiss($key): ?array {
    if (\str_starts_with($key, '/group/')) {
      $value = $this->doGetDomainInfoByPath($key);
    }
    else {
      $value = $this->doGetBasePathByDomain($key);
      // Unify data type.
      if ($value !== NULL) {
        $value = [$value];
      }
    }

    $this->set($key, $value);
    return $value;
  }

  /**
   * Lookup group base path by custom domain.
   *
   * @return string|null
   *   The base path.
   */
  private function doGetBasePathByDomain(string $domain): ?string {
    $domain = Settings::get('group_domain_mapping', [])[$domain] ?? $domain;
    $group_storage = $this->entityTypeManager->getStorage('group');
    try {
      $result = $group_storage->getQuery()
        ->accessCheck(FALSE)
        ->condition(self::DOMAIN_FIELD_NAME, $domain)
        ->range(0, 1)
        ->execute();
    }
    catch (QueryException $e) {
      // It can happen that the field is not installed sometimes.
      $result = [];
    }

    $group_id = \current($result);
    if ($group_id === FALSE) {
      return NULL;
    }

    $group = $group_storage->load($group_id);
    $base_path = '/' . $group->toUrl()->getInternalPath();
    $this->set($base_path, $this->domainGroupInfo($domain, $group));
    return $base_path;
  }

  /**
   * Lookup custom domain by path.
   *
   * @return array<string, string>
   *   Domain and group type array.
   */
  private function doGetDomainInfoByPath(string $path): ?array {
    $args = \explode('/', $path);

    if (!\array_key_exists(1, $args)) {
      return NULL;
    }

    $group_id = NULL;
    if ($args[1] === 'group' && \array_key_exists(2, $args) && \is_numeric($args[2])) {
      $group_id = $args[2];
    }

    if ($group_id === NULL) {
      return NULL;
    }

    $group = $this->entityTypeManager->getStorage('group')->load($group_id);
    if (!$group instanceof GroupInterface || !$group->hasField(self::DOMAIN_FIELD_NAME)) {
      return NULL;
    }

    $domain_field = $group->get(self::DOMAIN_FIELD_NAME);
    if ($domain_field->isEmpty()) {
      return NULL;
    }

    // When there is a domain mapping, we need to replace the domain in the
    // field value with the one in the mapping.
    $mapping = Settings::get('group_domain_mapping', []);
    $value = array_search($domain_field->value, $mapping, TRUE);
    $domain = $value ?: $domain_field->value;

    return $this->domainGroupInfo($domain, $group);
  }

  /**
   * Gets group for the current request.
   */
  public function getCurrentDomainGroup(): ?GroupInterface {
    // This method can be called multiple times but both CacheCollector and
    // EntityStorageBase use a static cache so no need to optimize.
    $request = $this->requestStack->getCurrentRequest();
    return $request === NULL ? NULL : $this->getRequestDomainGroup($request);
  }

  /**
   * Gets group for the provided request domain.
   */
  public function getRequestDomainGroup(Request $request): ?GroupInterface {
    if ($request === NULL) {
      return NULL;
    }
    $http_host = $request->getHttpHost();

    $http_host = Settings::get('group_domain_mapping', [])[$http_host] ?? $http_host;
    $base_group_path = $this->getBasePathByDomain($http_host);
    if ($base_group_path === NULL) {
      return NULL;
    }

    $group_id = \explode('/', $base_group_path)[2];
    return $this->entityTypeManager->getStorage('group')->load($group_id);
  }

}
