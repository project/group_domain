<?php

declare(strict_types=1);

namespace Drupal\group_domain\Form;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\group_domain\GroupDomainInfo;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the group domain settings form.
 */
final class GroupDomainSettingsForm extends ConfigFormBase {

  /**
   * The group domain info service.
   */
  private GroupDomainInfo $groupDomainInfo;

  /**
   * The entity type manager.
   */
  private EntityTypeManagerInterface $entityTypeManager;

  /**
   * The entity field manager.
   */
  private EntityFieldManagerInterface $entityFieldManager;

  /**
   * The render cache.
   */
  private CacheBackendInterface $cacheRender;

  /**
   * Per group type config.
   */
  private array $groupTypeConfig = [];

  /**
   * Constructs a new GroupDomainSettingsForm object.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    GroupDomainInfo $group_domain_info,
    EntityTypeManagerInterface $entity_type_manager,
    EntityFieldManagerInterface $entity_field_manager,
    CacheBackendInterface $cache_render
  ) {
    parent::__construct($config_factory);

    $this->groupDomainInfo = $group_domain_info;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->cacheRender = $cache_render;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('config.factory'),
      $container->get('group_domain.info'),
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('cache.render')
    );
  }

  /**
   * {@inheritdoc}
   *
   * @return string[]
   *   Editable Config Names.
   */
  protected function getEditableConfigNames(): array {
    return [GroupDomainInfo::CONFIG_NAME];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'group_domain_settings';
  }

  /**
   * {@inheritdoc}
   *
   * @return mixed[]
   *   The form.
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config(GroupDomainInfo::CONFIG_NAME);
    $group_types = $this->entityTypeManager->getStorage('group_type')->loadMultiple();

    $form['main_domain'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Main domain'),
      '#description' => $this->t('Without https://, example: "www.example.com".'),
      '#default_value' => $config->get('main_domain'),
    ];

    $form['group_type_config'] = [
      '#type' => 'container',
      '#tree' => TRUE,
    ];

    foreach ($group_types as $group_type) {
      $bundle = $group_type->id();
      $bundle_config = $this->groupDomainInfo->getConfigEntity($bundle);
      $this->groupTypeConfig[$bundle] = $bundle_config;

      $form['group_type_config'][$bundle] = [
        '#type' => 'fieldset',
        '#title' => $group_type->label(),
      ];

      $form['group_type_config'][$bundle]['status'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Enable'),
        '#default_value' => $bundle_config->get('status'),
      ];
      $form['group_type_config'][$bundle]['outbound_behavior'] = [
        '#type' => 'radios',
        '#title' => $this->t('Outbound links behavior'),
        '#description' => $this->t('Controls behavior of links on the main domain. If set to "Convert", and a group has custom domain set, links to that group will leac to that custom domain (for example https://custom-group-domain.com/content/1), otherwise user will stay on the main domain (links like https://main-domain.com/group/1/content/1).'),
        '#options' => [
          GroupDomainInfo::IGNORE_MAIN_DOMAIN_OUTBOUND => $this->t("Don't convert"),
          GroupDomainInfo::CONVERT_MAIN_DOMAIN_OUTBOUND => $this->t("Convert"),
        ],
        '#default_value' => $bundle_config->get('outbound_behavior'),
      ];
      $form['group_type_config'][$bundle]['add_membership_on_registration'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Add group membership on domain registration'),
        '#default_value' => $bundle_config->get('add_membership_on_registration'),
      ];
      $form['group_type_config'][$bundle]['registration_membership_roles'] = [
        '#title' => $this->t('Membership roles to add'),
        '#type' => 'checkboxes',
        '#options' => [],
        '#default_value' => $bundle_config->get('registration_membership_roles') ?? [],
        '#states' => [
          'visible' => [
            ':input[name="group_type_config[' . $bundle . '][add_membership_on_registration]"]' => ['checked' => TRUE],
          ],
        ],
      ];
      foreach ($group_type->getRoles(FALSE) as $role) {
        $form['group_type_config'][$bundle]['registration_membership_roles']['#options'][$role->id()] = $role->label();
      }
    }

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    if (\count($group_types) === 0) {
      $form['info'] = [
        '#weight' => -1,
        '#markup' => $this->t('There are no group types at the moment.'),
      ];
      $form['actions']['submit']['#disabled'] = TRUE;
      return $form;
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    parent::submitForm($form, $form_state);

    $config = $this->config(GroupDomainInfo::CONFIG_NAME);
    // Save general config.
    $config = $this->config(GroupDomainInfo::CONFIG_NAME);
    $config->set('main_domain', $form_state->getValue('main_domain'));
    $config->save();

    // Save group type config.
    $new_types = [];
    foreach ($form_state->getValue('group_type_config') as $group_type => $values) {
      $values['registration_membership_roles'] = \array_filter(\array_values($values['registration_membership_roles']));

      if ($values['status'] === 1) {
        $new_types[$group_type] = $group_type;
      }

      $config_entity = $this->groupTypeConfig[$group_type];
      foreach ($values as $key => $value) {
        $config_entity->set($key, $value);
      }
      if (!$config_entity->empty()) {
        $config_entity->save();
      }
      elseif (!$config_entity->isNew()) {
        $config_entity->delete();
      }
    }

    $old_types = [];
    $group_types = $this->entityTypeManager->getStorage('group_type')->loadMultiple();
    foreach ($group_types as $group_type) {
      $group_type_id = $group_type->id();
      $fields = $this->entityFieldManager->getFieldDefinitions('group', $group_type_id);
      if (\array_key_exists(GroupDomainInfo::DOMAIN_FIELD_NAME, $fields)) {
        $old_types[$group_type_id] = $group_type_id;
      }
    }

    if ($new_types === $old_types) {
      return;
    }

    // Update field definitions.
    $field_config_storage = $this->entityTypeManager->getStorage('field_config');
    $entity_form_display_storage = $this->entityTypeManager->getStorage('entity_form_display');
    $field_storage_config = $this->entityTypeManager->getStorage('field_storage_config')->load('group.' . GroupDomainInfo::DOMAIN_FIELD_NAME);

    // Handle additions.
    foreach ($new_types as $group_type_id) {
      if (!\in_array($group_type_id, $old_types, TRUE)) {
        $instances = $field_config_storage->loadByProperties([
          'entity_type' => 'group',
          'bundle' => $group_type_id,
          'field_name' => GroupDomainInfo::DOMAIN_FIELD_NAME,
        ]);
        if (\count($instances) > 0) {
          continue;
        }
        $field_config_storage->create([
          'field_storage' => $field_storage_config,
          'bundle' => $group_type_id,
          'label' => $this->t('Domain'),
          'description' => $this->t('Enter domain name without http:// or https:// (www.example.com).'),
        ])->save();

        // Build or retrieve the 'default' form mode and add the domain field.
        if (!$form_display = $entity_form_display_storage->load("group.$group_type_id.default")) {
          $form_display = $entity_form_display_storage->create([
            'targetEntityType' => 'group',
            'bundle' => $group_type_id,
            'mode' => 'default',
            'status' => TRUE,
          ]);
        }
        $form_display->setComponent(GroupDomainInfo::DOMAIN_FIELD_NAME, [
          'type' => 'text_textfield',
        ])->save();
      }
    }

    // Handle deletions.
    $group_storage = $this->entityTypeManager->getStorage('group');
    foreach ($old_types as $group_type_id) {
      if (!\in_array($group_type_id, $new_types, TRUE)) {
        // Remove existing field values as it's not enough to just delete
        // field instance config entities, values still would stay in the db.
        $result = $group_storage->getQuery()
          ->condition('type', $group_type_id)
          ->exists(GroupDomainInfo::DOMAIN_FIELD_NAME)
          ->condition(GroupDomainInfo::DOMAIN_FIELD_NAME, NULL, '!=')
          ->accessCheck()
          ->execute();
        foreach ($result as $group_id) {
          $group = $group_storage->load($group_id);
          $group->get(GroupDomainInfo::DOMAIN_FIELD_NAME)->removeItem(0);
          $group->save();
        }

        $instances = $field_config_storage->loadByProperties([
          'entity_type' => 'group',
          'bundle' => $group_type_id,
          'field_name' => GroupDomainInfo::DOMAIN_FIELD_NAME,
        ]);
        foreach ($instances as $instance) {
          $instance->delete();
        }
      }
    }

    // Finally clear render cache.
    $this->cacheRender->invalidateAll();
  }

}
