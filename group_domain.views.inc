<?php

/**
 * @file
 * Provide views data that isn't tied to any other module.
 */

declare(strict_types=1);

/**
 * Implements hook_views_data().
 */
function group_domain_views_data(): array {
  $data = [];

  $entity_type_definitions = \Drupal::entityTypeManager()->getDefinitions();
  foreach ($entity_type_definitions as $definition) {
    $base_table = $definition->getBaseTable();
    if ($base_table === NULL || $base_table === FALSE) {
      continue;
    }

    $title = t('Current domain group @entity_type.', [
      '@entity_type' => $definition->getPluralLabel(),
    ]);

    $data[$base_table]['domain_group_content'] = [
      'title' => $title,
      'filter' => [
        'title' => $title,
        'real field' => 'id',
        'id' => 'domain_group_content',
        'description' => t('Show only entities that are referenced by the current domain group content.'),
      ],
    ];
  }

  return $data;
}
